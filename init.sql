create table "meteorological station"
(
    id          serial  not null
        constraint "meteorological station_pk"
            primary key,
    temperature integer not null,
    humidity    integer not null,
    timestamp   timestamp default CURRENT_TIMESTAMP
);

alter table "meteorological station"
    owner to postgres;

create unique index "meteorological station_id_uindex"
    on "meteorological station" (id);