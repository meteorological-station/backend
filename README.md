### Init DB
For begin working with this project you should install Docker and then write commands:
```bash
# For initial all images from project to you're docker images
docker-compose -f ./stack.yaml build
# To start up DB
docker-compose -f ./stack.yaml up  
# To disable the database
docker-compose -f ./stack.yaml down  
```
### Start server
After initial step just start project with help Intellijidea